function calculadora(){
    const operacao = Number(prompt('escolha uma operação:\n 1 - soma (+)\n 2 - subtração (-)\n 3 - multiplicação (*)\n 4 - divisão real (/)\n 5 - divisão inteira (%)\n 6 - potenciação (**)'));

    let n1 = Number(prompt('Insira o primeiro valor:'));
    let n2 = Number(prompt('Insira o segundo valor:'));
    let resultado;

    function soma() {
        resultado = n1 + n2;
        alert(`${n1} + ${n2} = ${resultado}`);
    }

    function subtracao() {
        resultado = n1 - n2;
        alert(`${n1} - ${n2} = ${resultado}`);
    }

    function multiplicacao() {
        resultado = n1 * n2;
        alert(`${n1} * ${n2} = ${resultado}`);
    }

    function divisaoreal() {
        resultado = n1 / n2;
        alert(`${n1} / ${n2} = ${resultado}`);
    }

    function divisaointeira() {
        resultado = n1 % n2;
        alert(`O resto da divisão entre ${n1} e ${n2} é igual a ${resultado}`);
    }

    function potenciacao() {
        resultado = n1 + n2;
        alert(`${n1} elevado a  ${n2}ª é igual a ${resultado}`);
    }

    switch (operacao) {
        case 1:
            soma();
            break;
        case 2:
            subtracao()
            break;
        case 3:
            multiplicacao();
            break;
        case 4:
            divisaoreal();
            break;
        case 5:
            divisaointeira();
            break;
        case 6:
            potenciacao();
            break;
        default:
            alert("escolha uma operação válida");
            calculadora();
            break;
    }
}

calculadora();